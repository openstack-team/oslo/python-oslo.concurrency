python-oslo.concurrency (7.1.0-2) experimental; urgency=medium

  * d/watch: switch to version=4 and mode=git.

 -- Thomas Goirand <zigo@debian.org>  Tue, 25 Feb 2025 14:40:05 +0100

python-oslo.concurrency (7.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 24 Feb 2025 13:55:39 +0100

python-oslo.concurrency (6.1.0-4) unstable; urgency=medium

  * Blacklist (Closes: #1091608):
    - test_lockutils.LockTestCase.test_lock_externally_lock_dir_not_exist

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Jan 2025 10:34:28 +0100

python-oslo.concurrency (6.1.0-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090564).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 09:12:39 +0100

python-oslo.concurrency (6.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Sep 2024 17:45:34 +0200

python-oslo.concurrency (6.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Aug 2024 14:45:58 +0200

python-oslo.concurrency (6.0.0-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 03 Apr 2024 16:28:30 +0200

python-oslo.concurrency (6.0.0-2) experimental; urgency=medium

  * Also blacklist test_core_size (Closes: #1056322).

 -- Thomas Goirand <zigo@debian.org>  Mon, 11 Mar 2024 09:47:01 +0100

python-oslo.concurrency (6.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Feb 2024 16:29:55 +0100

python-oslo.concurrency (5.3.0-1) experimental; urgency=medium

  * New uptream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 24 Feb 2024 21:22:43 +0100

python-oslo.concurrency (5.2.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 13:10:02 +0200

python-oslo.concurrency (5.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Cleans even better.

 -- Thomas Goirand <zigo@debian.org>  Thu, 31 Aug 2023 10:22:58 +0200

python-oslo.concurrency (5.1.1-3) unstable; urgency=medium

  * Cleans better (Closes: #1045418).
  * Blacklist test_setrlimit_error().

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Aug 2023 12:41:57 +0200

python-oslo.concurrency (5.1.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 11:19:42 +0200

python-oslo.concurrency (5.1.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Feb 2023 10:15:09 +0100

python-oslo.concurrency (5.0.1-3) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 13:43:40 +0200

python-oslo.concurrency (5.0.1-2) experimental; urgency=medium

  * Re-enable remove-TestInternalLock.patch, and use, to run tests:
    TEST_EVENTLET=0 lockutils-wrapper.

 -- Thomas Goirand <zigo@debian.org>  Mon, 12 Sep 2022 16:50:20 +0200

python-oslo.concurrency (5.0.1-1) experimental; urgency=medium

  * New upstream release.
  * Add remove-TestInternalLock.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 12 Sep 2022 10:28:26 +0200

python-oslo.concurrency (5.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 29 Aug 2022 15:47:08 +0200

python-oslo.concurrency (4.5.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Mar 2022 15:11:51 +0100

python-oslo.concurrency (4.5.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Feb 2022 11:22:05 +0100

python-oslo.concurrency (4.4.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 08:55:41 +0200

python-oslo.concurrency (4.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed version of python3-openstackdocstheme.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Mar 2021 09:01:13 +0100

python-oslo.concurrency (4.3.1-1) unstable; urgency=medium

  * New upstream point release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 19 Jan 2021 10:20:40 +0100

python-oslo.concurrency (4.3.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Fix d/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2020 23:24:01 +0200

python-oslo.concurrency (4.3.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 13 Sep 2020 15:42:35 +0200

python-oslo.concurrency (4.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed python3-six from (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Sep 2020 20:49:03 +0200

python-oslo.concurrency (4.0.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 21:52:00 +0200

python-oslo.concurrency (4.0.2-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Apr 2020 14:27:17 +0200

python-oslo.concurrency (3.30.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Fix wrong Vcs-*.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Oct 2019 00:10:59 +0200

python-oslo.concurrency (3.30.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Added python3-sphinxcontrib.apidoc as build-depends.

 -- Thomas Goirand <zigo@debian.org>  Wed, 11 Sep 2019 17:10:10 +0200

python-oslo.concurrency (3.29.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 01:56:17 +0200

python-oslo.concurrency (3.29.1-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.
  * Removed installation of alternatives.

 -- Thomas Goirand <zigo@debian.org>  Wed, 20 Mar 2019 15:35:20 +0100

python-oslo.concurrency (3.27.0-3) unstable; urgency=medium

  * Removed python{3,}-posix-ipc as (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Fri, 07 Sep 2018 11:28:23 +0200

python-oslo.concurrency (3.27.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 03 Sep 2018 16:52:58 +0200

python-oslo.concurrency (3.27.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 19 Aug 2018 12:12:10 +0200

python-oslo.concurrency (3.25.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 21:15:05 +0000

python-oslo.concurrency (3.25.0-1) experimental; urgency=medium

  * Set VCS URLs to point to salsa.
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 10 Feb 2018 14:19:47 +0100

python-oslo.concurrency (3.21.0-3) unstable; urgency=medium

  * Apply Ubuntu patch to fix unit tests on i386 (those tests which I disabled
    in previous Debian release). Now running all tests again. (Closes: #880597)

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Nov 2017 22:13:09 +0100

python-oslo.concurrency (3.21.0-2) unstable; urgency=medium

  * Disable some unit tests failing on i386 (Closes: #880227).
  * Using pkgos-dh_auto_test.
  * Watch file using https.
  * Standards-Version is now 4.1.1.
  * Using pkgos-dh_auto_install.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 Nov 2017 21:10:59 +0000

python-oslo.concurrency (3.21.0-1) experimental; urgency=medium

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed all patches: either not relevant or applied upstream.
  * debian/gbp.conf to use debian/pike as packaging branch.
  * Removed now useless transition packages.
  * Fixed debian/copyright holders.

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Correcting permissions in debian packaging files.
  * Updating standards version to 4.0.1.
  * Deprecating priority extra as per policy 4.0.1.
  * Updating standards version to 4.1.0.

 -- Thomas Goirand <zigo@debian.org>  Fri, 04 Aug 2017 19:34:28 +0000

python-oslo.concurrency (3.7.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Changed UPSTREAM_GIT protocol to https

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 21 Jun 2016 11:07:44 +0200

python-oslo.concurrency (3.7.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 04 Apr 2016 08:42:17 +0000

python-oslo.concurrency (3.7.0-1) experimental; urgency=medium

  [ Corey Bryant ]
  * d/p/fix-tests-i386.patch: Fix tests that fail on i386 builds.

  [ Thomas Goirand ]
  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 02 Apr 2016 14:17:31 +0200

python-oslo.concurrency (3.6.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).

  [ Corey Bryant ]
  * New upstream release.
  * d/control: Align (Build-)Depends with upstream.
  * d/p/lock-path-tempfile.patch: Cherry picked from upstream to set default
    of lock_file to a tempfile if OSLO_LOCK_PATH isn't set (LP: #1550188).

  [ Thomas Goirand ]
  * Fixed debian/copyright ordering.
  * Standards-Version: 3.9.7 (no change).

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Mar 2016 10:45:58 +0000

python-oslo.concurrency (3.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Jan 2016 03:27:02 +0000

python-oslo.concurrency (3.2.0-1) experimental; urgency=medium

  * New upstream release.
  * Also run tests in Python 3.5.
  * Removed all patches.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 Jan 2016 01:36:33 +0000

python-oslo.concurrency (3.0.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 01 Dec 2015 10:53:33 +0100

python-oslo.concurrency (2.6.0-3) unstable; urgency=medium

  * override_dh_python3 to fix Py3 shebang.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Oct 2015 23:22:37 +0000

python-oslo.concurrency (2.6.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 15 Oct 2015 19:22:21 +0000

python-oslo.concurrency (2.6.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Don't test with Python 3.5 for now (only 3.4) as there's unit test
    failures.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Sep 2015 17:12:06 +0200

python-oslo.concurrency (2.3.0-1) experimental; urgency=medium

  [ James Page ]
  * Fixup typo in transitional package description (LP: #1471561).

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed build-depends for this release.
  * Stop the manual copy of now inexistant folder.
  * Added Priority: extra to transition packages.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Jul 2015 23:57:32 +0000

python-oslo.concurrency (2.0.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Update watch file to use Debian pypi redirector.

 -- James Page <james.page@ubuntu.com>  Wed, 10 Jun 2015 17:48:56 +0100

python-oslo.concurrency (1.10.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release:
    - d/control: Align version requirements with upstream.
  * Re-align with Ubuntu:
    - d/control: Add Breaks/Replaces and transitional packages for
      *oslo-concurrency packages in Ubuntu.

 -- James Page <james.page@ubuntu.com>  Tue, 09 Jun 2015 08:49:55 +0100

python-oslo.concurrency (1.8.0-1) unstable; urgency=medium

  * New upstream release.
  * Added python-retrying as (build-)depends.
  * Copy the openstack-common stuff manually, as it's not there.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Dec 2014 10:44:01 +0800

python-oslo.concurrency (0.2.0-1) unstable; urgency=medium

  * Initial release. (Closes: #769287)

 -- Thomas Goirand <zigo@debian.org>  Wed, 12 Nov 2014 20:49:34 +0800
